<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lfp extends Model
{
    use HasFactory;
    protected $table = 'lfp_data';
    protected $guarded = ['id'];
    protected $fillable = [
        'PMO',
        'Byear',
        'province',
        'city_mun',
        'project',
        'brgy',
        'allocation',
        'CO_Code',
        'column_2',
        'pa_percent',
        'obligation_rate',
        'disbursement_rate',
        'Liquidation_Rate',
        'for_ongoing',
        'total_project_completion',
        'status',
        'remarks',
        'rem',

    ];

}
