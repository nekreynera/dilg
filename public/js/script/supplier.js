$(document).ready(function(){

    /*edit button*/
    $("#edit_btn").click(function () {
        $(this).addClass('visually-hidden');
        $('#control_btns').removeClass('visually-hidden');
        $("#supplier_form input,textarea").removeAttr('readonly');
    });
    //New supplier buttons
    $('#NewSupplier').click(function(){

        $('#supplier_id,#company_name,#address,#email,#contact_person,#contact_no,#alternate_contact_no').val('');
    });
    //View button
    $(".view").each(function () {
        $(this).click(function () {
            $('#staticBackdrop').modal('show');
            $('#name').val($(this).closest('tr').find("td:eq(0)").attr('attr-name'));
            $('#email').val($(this).closest('tr').find("td:eq(1)").attr('attr-email'));
            $('#id').val($(this).closest('tr').find("td:eq(0)").attr('attr-id'));
            $('#remove-user').removeClass('d-none');
        });
    });

    //modal close
    $("#staticBackdrop").on("hidden.bs.modal", function () {
        $('#remove-supplier').addClass('d-none');
    });

    //remove buttons
    $('#remove-user').click(function(){
        $('#ConfirmationModalAction').attr('action','UserAccounts/'+$('#id').val());
        $('#RemoveConfimationModal').modal('show');

    });

    //Save Supplier
    $('#SaveUserForm').on('submit', function(e){
        var errorCount = 0;
        if(!$('#name').val()){
            errorCount=errorCount+1;
            $('#name').addClass('is-invalid');
        }else{
            $('#name').removeClass('is-invalid');
        }
        if(!$('#email').val()){
            errorCount=errorCount+1;
            $('#email').addClass('is-invalid');
        }else{
            $('#email').removeClass('is-invalid');
        }

        if(errorCount > 0){
            console.log('cant submit');

            e.preventDefault();
        }else{
            //console.log('submit na sana!')
            return;
        }

    });

});
