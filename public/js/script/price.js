$(document).ready(function(){
    $(".breed_container").click(function () {

        $(this).css('background-color', '#ece7e4');
        $("#breed_id").val($(this).attr('attr-breed-id'));
        $("#price_id").val($(this).attr('attr-current-price-id'));

        $("#update_price_modal").modal('show');
        $("#newprice").focus();

    });

    $('#update_price_modal').on('hidden.bs.modal', function (e) {
        $(".breed_container").css('background-color','');
    })
});