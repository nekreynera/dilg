var bid,wrid="";
var ctr=1;
var index=0;

$(document).ready(function(){


    $('#table1').on('click', '.approve', function(){
        console.log($(this).attr('attr-id'));

        $.ajax({
            method      : "post",
            url         : '/update_request_order_status',
            data        : {
                _token:$("#_token").val(),
                id:$(this).attr('attr-id'),
                param:'approve'},
            success     : function(result){
                location.reload();
            }
        });
    });

    $('#table1').on('click', '.cancel', function(){
        console.log($(this).attr('attr-id'));

        $.ajax({
            method      : "post",
            url         : '/update_request_order_status',
            data        : {
                _token:$("#_token").val(),
                id:$(this).attr('attr-id'),
                param:'cancel'},
            success     : function(result){
                location.reload();
            }
        });
    });

    /*$('#table1 tbody tr').each(function () {
       $(this).on('click',function(){
           console.log($(this).attr('attr-id'))
           window.open('request_order/'+$(this).attr('attr-id'),'blank');
       });
    });*/

    $('#table1 tbody tr').each(function () {
        /*$(this).hover(function(){
            console.log($(this).attr('attr-id'))
            $(this).css('text-decoration','underline');
            /!*window.open('request_order/'+$(this).attr('attr-id'),'blank');*!/
        });*/
        $(this).mouseout(function() {
                $(this).css({'text-decoration':'none','color':'#212529;','font-weight': 'normal'});
            }).mouseover(function() {
                $(this).css({'text-decoration':'underline','color':'#0c7645','font-weight':'bold'});
            });

    });
    $('#add').click(function(){
        if($("#breed_id").val() != null){
            if($("#weight_range_id").val() == null || $("#weight_range_id").val() == ""){
                $("#weight_range_id").addClass('is-invalid');
                $("#qty").removeClass('is-invalid');
            }else if($("#qty").val() == null || $("#qty").val() == "" || $("#qty").val() <=0){
                $("#qty").addClass('is-invalid');
                $("#weight_range_id").removeClass('is-invalid');
            }else{
                var sub = parseInt($("#price").val())*parseInt($("#qty").val());
                var markup = "<tr prod_id='"+$("#prod_id").val()+"'>" +
                    "<td>"+ctr+"</td>" +
                    "<td><input class='visually-hidden' type='text' name='breed_id[]' value='"+$('#breed_id').val()+"'>"+$("#breed_id option:selected").text()+"</td>" +
                    "<td><input class='visually-hidden' type='text' name='weight_range_id[]' value='"+$("#weight_range_id option:selected").text()+"'>"+$("#weight_range_id option:selected").text()+"</td>" +
                    "<td><input class='visually-hidden' type='text' name='qty[]' value='"+$("#qty").val()+"'>"+$("#qty").val()+"</td>" +
                    "<td><input class='visually-hidden' type='text' name='price[]' value='"+$("#price").val()+"'>"+$("#price").val()+"</td>" +
                        /*"<td class='subtotal' attr-subtotal='"+sub+"'><input class='visually-hidden' type='text' name='selling_price[]' value=''>"+sub.toLocaleString()+".00</td>" +*/
                    "<td><button type='button' class='btn icon btn-primary btn-sm remove_item'><i class='fa fa-trash'/></button></td></tr>";
                $("#request_list tbody").append(markup);

                $("#breed_id,#weight_range_id").prop("selectedIndex", 0);
                $("#qty,#price").val("");
                $("#submit-btn").attr('disabled',false);
                calc_total();
                $("#breed_id,#weight_range_id,#qty").removeClass('is-invalid');
            }
            $("#breed_id").removeClass('is-invalid');
        }else{
            $("#breed_id").addClass('is-invalid');
        }




    });

   $("#supplier").change(function(){
       $.ajax({
           method      : "get",
           url         : '/supplier_info',
           data        : {
               _token:$("#_token").val(),
               id:$(this).val()},
           success     : function(result){
               var data = $.parseJSON(result);
               console.log(data);
               $("#breed option").remove();
               $("#owner_name").val(data.supplier_info.owner_name);
               $("#contact_no").val(data.supplier_info.contact_no);
               $("#email").val(data.supplier_info.email);
               $("#contact_person").val(data.supplier_info.contact_person);
               $("#address").val(data.supplier_info.address);
               $("#breed").append('<option selected disabled>Select here</option>')
               for(var x=0;x<data.breeds.length;x++){

                       $("#breed").append("<option value='"+data.breeds[x].breed.id+"'>"+data.breeds[x].breed.breed_name+"</option>");



               }
               $("#breed").focus();
           }
       });
   })

    $("#breed_id").change(function () {
        bid=$(this).val();

            getprice();
        console.log('aaaaa');


    });

    $("#request_list tbody").on('click', 'button.remove_item', function() {

        index = $(this).closest('tr').index();
        $("#request_list tbody tr:eq("+index+")").remove();
        var trows = $("#request_list>tbody>tr").length;

        if(trows>0){
            $("#submit-btn").attr('disabled',false);
        }else{
            //$("#prod_id option").remove();
            $("#submit-btn").attr('disabled',true);
        }

    });

    $("#qty").inputFilter(function(value) {
        return /^\d*$/.test(value);    // Allow digits only, using a RegExp
    });


});

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));
function getprice(){
    $("#price").val('');
    $.ajax({
        method      : "get",
        url         : '/get_price',
        data        : {
            _token:$("#_token").val(),
            breed_id:bid,
            weight_range_id:wrid},
        success     : function(result){
            var data = $.parseJSON(result);
            if(data.length>0){
                $("#price").val(data[0].price);
            }
        }
    });
}
function calc_total(){
    var sum = 0;
    $("#request_list tbody tr td.subtotal").each(function(){
        sum += parseFloat($(this).attr('attr-subtotal'));

    });
    $('#sum').text(sum.toLocaleString()+".00");
}
