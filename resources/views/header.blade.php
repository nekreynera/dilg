<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ url('assets/vendors/simple-datatables/style.css') }}">
<link rel="stylesheet" href="{{ url('assets/vendors/chartjs/Chart.min.css') }}">
<link rel="stylesheet" href="{{ url('css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ url('assets/css/app.css') }}">
<link rel="stylesheet" href="{{url('css/style.css')}}">
{{--<link rel="shortcut icon" href="assets/images/favicon.svg" type="image/x-icon">--}}
