@extends('app')
@section('scripts')
    <script src="{{url('js/script/supplier.js')}}"></script>
@endsection
@section('style')
    <style type="text/css">
       .is-invalid{
           border-color: red;
       }
    </style>
@endsection
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header" style="background-color: transparent;!important;">
                <h2><i class="fas fa-users fa-lg"></i> Accounts</h2>
                <button class="btn btn-danger pull-right" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" id='NewSupplier'><i class="fa fa-plus"></i>New</button>

            </div>
            <div class="card-body">
                <!-- Button trigger modal -->


                <table class='table table-striped' id="table1">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>ACTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $a)
                        <tr>
                            {{--<td attr-id="{{$s->id}}" attr-farm_name="{{$s->farm_name}}">
                                <a href="{{url('supplier/'.$s->id)}}">{{$s->farm_name}}</a>
                            </td>
                            <td attr-address="{{$s->address}}">{{$s->address}}</td>
                            <td attr-owner_name="{{$s->owner_name}}">{{$s->owner_name}}</td>
                            <td attr-contact_person="{{$s->contact_person}}">{{$s->contact_person}}</td>--}}
                            <td attr-id="{{$a->id}}" attr-name="{{$a->name}}" >{{$a->name}}</td>
                            <td attr-email="{{$a->email}}">{{$a->email}}</td>
                            <td class="text-center"><a href="#" class="view"><i class="fas fa-eye"></i></a></td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        </div>

    </section>

    <!-- Modal -->
    <form action="{{url('UserAccounts')}}" method="POST" id='SaveUserForm'>
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">User information</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="first-name-vertical">Name</label>
                                <textarea class="form-control" id="name" name="name" rows="3" spellcheck="false" placeholder="Account Name"></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="first-name-vertical">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter email">
                            </div>
                        </div>{{--
                        <div class="col-12">
                            <div class="form-group">
                                <label for="first-name-vertical">Contact Person</label>
                                <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Contact Person">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="first-name-vertical">Contact No.</label>
                                <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No.">
                            </div>
                        </div>--}}
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-danger d-none" id="remove-user">Remove</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id='save'>Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--Delete Confimation Modal-->

    <form method="post" action="{{url('UserAccounts/')}}" id="ConfirmationModalAction">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
        <div class="modal fade" id="RemoveConfimationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-2" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="smallmodalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to this account?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success" id="confirm-delete">
                            <i class="fa fa-check"></i>
                            Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
