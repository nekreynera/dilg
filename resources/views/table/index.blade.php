@extends('app')
@section('scripts')
    <script src="{{url('js/script/supplier.js')}}"></script>
@endsection
@section('style')
    <style type="text/css">
        .is-invalid{
            border-color: red;
        }
    </style>
@endsection
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header" style="background-color: transparent;!important;">
                <h4><i class="fas fa-list-alt fa-lg"></i>Summary|Completed SPs | As of June 28, 2019</h4>


            </div>
            <div class="card-body">
                <!-- Button trigger modal -->


                <table class='table table-striped' id="table1">
                    <thead>
                    <tr>
                        <th>Program</th>
                        <th>Year</th>
                        <th>Province</th>
                        <th>Municipality/LGU</th>
                        <th>Barangay</th>
                        <th>Project</th>
                        <th>Allocation</th>
                        <th>Status</th>
                        <th>PA%</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $d)
                        <tr>

                            <td >{{$d->PMO}}</td>
                            <td >{{$d->Byear}}</td>
                            <td >{{$d->province}}</td>
                            <td >{{$d->city_mun}}</td>
                            <td >{{$d->brgy}}</td>
                            <td >{{$d->project}}</td>
                            <td >{{$d->allocation}}</td>
                            <td >{{$d->status}}</td>
                            <td >{{$d->pa_percent}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        </div>

    </section>


@endsection
