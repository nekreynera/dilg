<?php
use Khill\Lavacharts\Lavacharts;
use App\Models\Lfp;
$lava = new Lavacharts;

$reasons = $lava->DataTable();
$data = Lfp::all('project','allocation')->where('province','=','samar')->where('status','=','COMPLETED')->toArray();





$reasons->addStringColumn('Reasons')
        ->addNumberColumn('Percent')
        ->addRow(['Cancelled', 6.07])
        ->addRow(['Under Prep/Not Yet Started', 0.19])
        ->addRow(['DED Prep', 0.57])
        ->addRow(['PRE Procurement', 5.12])
        ->addRow(['On-Going', 10.25])
        ->addRow(['Completed', 77.80]);

$lava->PieChart('IMDB', $reasons, [
        'title'  => 'Samar | 527 Sps',
        'is3D'   => false,
        {{--'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
        ],--}}
        {{--'width' => 1000,--}}
        'height'=>700
]);
?>
@extends('app')
@section('scripts')
    <script src="{{url('js/script/supplier.js')}}"></script>
@endsection
@section('style')
    <style type="text/css">
        .is-invalid{
            border-color: red;
        }
    </style>
@endsection
@section('content')
    <section class="section">
        <div class="card">
            <div id="samar-div"></div>
            <?= $lava->render('PieChart', 'IMDB', 'samar-div') ?>
            <?php
            {{--$totalsamar = Lfp::where('province','=','samar')->count();
            $samarcompleted = Lfp::where('province','=','samar')->where('status','=','COMPLETED')->count();
            $samarCANCELLED = Lfp::where('province','=','samar')->where('status','=','CANCELLED')->count();
            $samarongoing = Lfp::where('province','=','samar')->where('status','=','ON-GOING')->count();
            $samarCANCELLED = Lfp::where('province','=','samar')->where('status','=','CANCELLED')->count();

            echo "totalsamar=".$totalsamar."<br>";
            echo "totalsamar_completed=".$samarcompleted."<br>";
            echo "CANCELLED=".$samarCANCELLED."<br>";
            echo "ON-GOING=".$samarongoing."<br>";--}}
            ?>
            <div id="samar-div"></div>
            <?= $lava->render('PieChart', 'IMDB', 'samar-div') ?>
        </div>
        </div>

    </section>


@endsection
