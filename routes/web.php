<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LfpController;
use App\Http\Controllers\GraphController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    //return view('pages.index');
    if(\Illuminate\Support\Facades\Auth::check()){
        //\Illuminate\Support\Facades\Storage::put('img', getEmployeeRoles());
        return view('welcome');
    }else{
        //\Illuminate\Support\Facades\Storage::put('img', '');
        return view('auth.login');
    }
});


/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('logout',function(){

    Auth::logout();

    return redirect('/');
});

//Route::get('UserAccounts',[App\Http\Controllers\UserController::class,'index'])->name('index');
Route::resource('UserAccounts', UserController::class);
Route::resource('Reports', LfpController::class);
Route::resource('GraphReport', GraphController::class);